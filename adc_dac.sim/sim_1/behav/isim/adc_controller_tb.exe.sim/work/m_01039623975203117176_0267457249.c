/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/rock/vlsi_lab/adc_dac/adc_dac.srcs/sources_1/new/adc_dac.v";
static unsigned int ng1[] = {1U, 0U};
static int ng2[] = {0, 0};
static int ng3[] = {1, 0};
static unsigned int ng4[] = {3U, 0U};
static unsigned int ng5[] = {6U, 0U};
static unsigned int ng6[] = {7U, 0U};
static unsigned int ng7[] = {0U, 0U};
static unsigned int ng8[] = {2U, 0U};
static unsigned int ng9[] = {16U, 0U};
static unsigned int ng10[] = {17U, 0U};
static int ng11[] = {7, 0};
static unsigned int ng12[] = {35U, 0U};
static unsigned int ng13[] = {4U, 0U};
static int ng14[] = {33, 0};
static unsigned int ng15[] = {36U, 0U};
static unsigned int ng16[] = {5U, 0U};
static int ng17[] = {15, 0};
static int ng18[] = {4, 0};
static unsigned int ng19[] = {48U, 0U};
static int ng20[] = {23, 0};
static int ng21[] = {16, 0};
static unsigned int ng22[] = {33U, 0U};
static int ng23[] = {32, 0};
static unsigned int ng24[] = {34U, 0U};



static void Always_42_0(char *t0)
{
    char t6[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    char *t28;
    char *t29;

LAB0:    t1 = (t0 + 8408U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(42, ng0);
    t2 = (t0 + 9720);
    *((int *)t2) = 1;
    t3 = (t0 + 8440);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(43, ng0);

LAB5:    xsi_set_current_line(45, ng0);
    t4 = (t0 + 2136U);
    t5 = *((char **)t4);
    t4 = ((char*)((ng1)));
    memset(t6, 0, 8);
    t7 = (t5 + 4);
    t8 = (t4 + 4);
    t9 = *((unsigned int *)t5);
    t10 = *((unsigned int *)t4);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t7);
    t13 = *((unsigned int *)t8);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t7);
    t17 = *((unsigned int *)t8);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB9;

LAB6:    if (t18 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t6) = 1;

LAB9:    t22 = (t6 + 4);
    t23 = *((unsigned int *)t22);
    t24 = (~(t23));
    t25 = *((unsigned int *)t6);
    t26 = (t25 & t24);
    t27 = (t26 != 0);
    if (t27 > 0)
        goto LAB10;

LAB11:    xsi_set_current_line(54, ng0);

LAB14:    xsi_set_current_line(55, ng0);
    t2 = (t0 + 7496);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng4)));
    memset(t6, 0, 8);
    t7 = (t4 + 4);
    if (*((unsigned int *)t7) != 0)
        goto LAB16;

LAB15:    t8 = (t5 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB16;

LAB19:    if (*((unsigned int *)t4) > *((unsigned int *)t5))
        goto LAB18;

LAB17:    *((unsigned int *)t6) = 1;

LAB18:    t22 = (t6 + 4);
    t9 = *((unsigned int *)t22);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB20;

LAB21:    xsi_set_current_line(59, ng0);
    t2 = (t0 + 7496);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng5)));
    memset(t6, 0, 8);
    t7 = (t4 + 4);
    if (*((unsigned int *)t7) != 0)
        goto LAB25;

LAB24:    t8 = (t5 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB25;

LAB28:    if (*((unsigned int *)t4) > *((unsigned int *)t5))
        goto LAB27;

LAB26:    *((unsigned int *)t6) = 1;

LAB27:    t22 = (t6 + 4);
    t9 = *((unsigned int *)t22);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB29;

LAB30:    xsi_set_current_line(63, ng0);
    t2 = (t0 + 7496);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng6)));
    memset(t6, 0, 8);
    t7 = (t4 + 4);
    t8 = (t5 + 4);
    t9 = *((unsigned int *)t4);
    t10 = *((unsigned int *)t5);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t7);
    t13 = *((unsigned int *)t8);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t7);
    t17 = *((unsigned int *)t8);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB36;

LAB33:    if (t18 != 0)
        goto LAB35;

LAB34:    *((unsigned int *)t6) = 1;

LAB36:    t22 = (t6 + 4);
    t23 = *((unsigned int *)t22);
    t24 = (~(t23));
    t25 = *((unsigned int *)t6);
    t26 = (t25 & t24);
    t27 = (t26 != 0);
    if (t27 > 0)
        goto LAB37;

LAB38:
LAB39:
LAB31:
LAB22:    xsi_set_current_line(68, ng0);
    t2 = (t0 + 7496);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng3)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 32, t4, 3, t5, 32);
    t7 = (t0 + 7496);
    xsi_vlogvar_wait_assign_value(t7, t6, 0, 0, 3, 0LL);

LAB12:    goto LAB2;

LAB8:    t21 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t21) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(46, ng0);

LAB13:    xsi_set_current_line(47, ng0);
    t28 = ((char*)((ng2)));
    t29 = (t0 + 7496);
    xsi_vlogvar_wait_assign_value(t29, t28, 0, 0, 3, 0LL);
    xsi_set_current_line(48, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 4776);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(50, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 5096);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(51, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5256);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    goto LAB12;

LAB16:    t21 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t21) = 1;
    goto LAB18;

LAB20:    xsi_set_current_line(56, ng0);

LAB23:    xsi_set_current_line(57, ng0);
    t28 = ((char*)((ng2)));
    t29 = (t0 + 6056);
    xsi_vlogvar_wait_assign_value(t29, t28, 0, 0, 1, 0LL);
    goto LAB22;

LAB25:    t21 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t21) = 1;
    goto LAB27;

LAB29:    xsi_set_current_line(60, ng0);

LAB32:    xsi_set_current_line(61, ng0);
    t28 = ((char*)((ng3)));
    t29 = (t0 + 6056);
    xsi_vlogvar_wait_assign_value(t29, t28, 0, 0, 1, 0LL);
    goto LAB31;

LAB35:    t21 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t21) = 1;
    goto LAB36;

LAB37:    xsi_set_current_line(64, ng0);

LAB40:    xsi_set_current_line(65, ng0);
    t28 = ((char*)((ng3)));
    t29 = (t0 + 6056);
    xsi_vlogvar_wait_assign_value(t29, t28, 0, 0, 1, 0LL);
    xsi_set_current_line(66, ng0);
    t2 = ((char*)((ng7)));
    t3 = (t0 + 7496);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    goto LAB39;

}

static void Always_73_1(char *t0)
{
    char t6[8];
    char t35[8];
    char t47[8];
    char t48[8];
    char t52[8];
    char t89[8];
    char t111[8];
    char t118[8];
    char t122[8];
    char t125[8];
    char t165[8];
    char t172[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    char *t28;
    char *t29;
    int t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t36;
    char *t37;
    char *t38;
    char *t39;
    unsigned int t40;
    unsigned int t41;
    unsigned int t42;
    unsigned int t43;
    unsigned int t44;
    char *t45;
    char *t46;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t53;
    unsigned int t54;
    unsigned int t55;
    char *t56;
    char *t57;
    char *t58;
    unsigned int t59;
    unsigned int t60;
    unsigned int t61;
    unsigned int t62;
    unsigned int t63;
    unsigned int t64;
    unsigned int t65;
    char *t66;
    char *t67;
    unsigned int t68;
    unsigned int t69;
    unsigned int t70;
    unsigned int t71;
    unsigned int t72;
    unsigned int t73;
    unsigned int t74;
    unsigned int t75;
    int t76;
    unsigned int t77;
    unsigned int t78;
    unsigned int t79;
    unsigned int t80;
    unsigned int t81;
    unsigned int t82;
    char *t83;
    unsigned int t84;
    unsigned int t85;
    unsigned int t86;
    unsigned int t87;
    unsigned int t88;
    char *t90;
    char *t91;
    char *t92;
    char *t93;
    unsigned int t94;
    unsigned int t95;
    unsigned int t96;
    unsigned int t97;
    unsigned int t98;
    char *t99;
    char *t100;
    char *t101;
    unsigned int t102;
    unsigned int t103;
    unsigned int t104;
    unsigned int t105;
    unsigned int t106;
    unsigned int t107;
    unsigned int t108;
    unsigned int t109;
    char *t110;
    char *t112;
    char *t113;
    char *t114;
    char *t115;
    char *t116;
    char *t117;
    char *t119;
    char *t120;
    char *t121;
    char *t123;
    char *t124;
    unsigned int t126;
    unsigned int t127;
    unsigned int t128;
    char *t129;
    char *t130;
    char *t131;
    unsigned int t132;
    unsigned int t133;
    unsigned int t134;
    unsigned int t135;
    unsigned int t136;
    unsigned int t137;
    unsigned int t138;
    char *t139;
    char *t140;
    unsigned int t141;
    unsigned int t142;
    unsigned int t143;
    unsigned int t144;
    unsigned int t145;
    unsigned int t146;
    unsigned int t147;
    unsigned int t148;
    int t149;
    int t150;
    unsigned int t151;
    unsigned int t152;
    unsigned int t153;
    unsigned int t154;
    unsigned int t155;
    unsigned int t156;
    char *t157;
    unsigned int t158;
    unsigned int t159;
    unsigned int t160;
    unsigned int t161;
    unsigned int t162;
    char *t163;
    char *t164;
    char *t166;
    char *t167;
    char *t168;
    char *t169;
    char *t170;
    char *t171;
    char *t173;
    int t174;
    int t175;
    int t176;
    int t177;

LAB0:    t1 = (t0 + 8656U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(73, ng0);
    t2 = (t0 + 9736);
    *((int *)t2) = 1;
    t3 = (t0 + 8688);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(73, ng0);

LAB5:    xsi_set_current_line(75, ng0);
    t4 = (t0 + 2616U);
    t5 = *((char **)t4);
    t4 = ((char*)((ng1)));
    memset(t6, 0, 8);
    t7 = (t5 + 4);
    t8 = (t4 + 4);
    t9 = *((unsigned int *)t5);
    t10 = *((unsigned int *)t4);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t7);
    t13 = *((unsigned int *)t8);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t7);
    t17 = *((unsigned int *)t8);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB9;

LAB6:    if (t18 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t6) = 1;

LAB9:    t22 = (t6 + 4);
    t23 = *((unsigned int *)t22);
    t24 = (~(t23));
    t25 = *((unsigned int *)t6);
    t26 = (t25 & t24);
    t27 = (t26 != 0);
    if (t27 > 0)
        goto LAB10;

LAB11:    xsi_set_current_line(89, ng0);
    t2 = (t0 + 2296U);
    t3 = *((char **)t2);
    t2 = ((char*)((ng1)));
    memset(t6, 0, 8);
    t4 = (t3 + 4);
    t5 = (t2 + 4);
    t9 = *((unsigned int *)t3);
    t10 = *((unsigned int *)t2);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t4);
    t13 = *((unsigned int *)t5);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t4);
    t17 = *((unsigned int *)t5);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB17;

LAB14:    if (t18 != 0)
        goto LAB16;

LAB15:    *((unsigned int *)t6) = 1;

LAB17:    t8 = (t6 + 4);
    t23 = *((unsigned int *)t8);
    t24 = (~(t23));
    t25 = *((unsigned int *)t6);
    t26 = (t25 & t24);
    t27 = (t26 != 0);
    if (t27 > 0)
        goto LAB18;

LAB19:    xsi_set_current_line(103, ng0);

LAB22:    xsi_set_current_line(104, ng0);
    t2 = (t0 + 6856);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);

LAB23:    t5 = ((char*)((ng7)));
    t30 = xsi_vlog_unsigned_case_compare(t4, 3, t5, 3);
    if (t30 == 1)
        goto LAB24;

LAB25:    t2 = ((char*)((ng1)));
    t30 = xsi_vlog_unsigned_case_compare(t4, 3, t2, 3);
    if (t30 == 1)
        goto LAB26;

LAB27:    t2 = ((char*)((ng8)));
    t30 = xsi_vlog_unsigned_case_compare(t4, 3, t2, 3);
    if (t30 == 1)
        goto LAB28;

LAB29:    t2 = ((char*)((ng4)));
    t30 = xsi_vlog_unsigned_case_compare(t4, 3, t2, 3);
    if (t30 == 1)
        goto LAB30;

LAB31:    t2 = ((char*)((ng13)));
    t30 = xsi_vlog_unsigned_case_compare(t4, 3, t2, 3);
    if (t30 == 1)
        goto LAB32;

LAB33:    t2 = ((char*)((ng16)));
    t30 = xsi_vlog_unsigned_case_compare(t4, 3, t2, 3);
    if (t30 == 1)
        goto LAB34;

LAB35:    t2 = ((char*)((ng5)));
    t30 = xsi_vlog_unsigned_case_compare(t4, 3, t2, 3);
    if (t30 == 1)
        goto LAB36;

LAB37:    t2 = ((char*)((ng6)));
    t30 = xsi_vlog_unsigned_case_compare(t4, 3, t2, 3);
    if (t30 == 1)
        goto LAB38;

LAB39:
LAB40:
LAB20:
LAB12:    goto LAB2;

LAB8:    t21 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t21) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(76, ng0);

LAB13:    xsi_set_current_line(77, ng0);
    t28 = ((char*)((ng1)));
    t29 = (t0 + 6856);
    xsi_vlogvar_wait_assign_value(t29, t28, 0, 0, 3, 0LL);
    xsi_set_current_line(78, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5576);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(79, ng0);
    t2 = ((char*)((ng7)));
    t3 = (t0 + 6216);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    xsi_set_current_line(80, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 4136);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(81, ng0);
    t2 = ((char*)((ng7)));
    t3 = (t0 + 4296);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 14);
    xsi_set_current_line(82, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5416);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(84, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 7176);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    xsi_set_current_line(85, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 5896);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(86, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5736);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    goto LAB12;

LAB16:    t7 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t7) = 1;
    goto LAB17;

LAB18:    xsi_set_current_line(89, ng0);

LAB21:    xsi_set_current_line(90, ng0);
    t21 = ((char*)((ng8)));
    t22 = (t0 + 6856);
    xsi_vlogvar_wait_assign_value(t22, t21, 0, 0, 3, 0LL);
    xsi_set_current_line(92, ng0);
    t2 = ((char*)((ng7)));
    t3 = (t0 + 6216);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    xsi_set_current_line(93, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 4136);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(94, ng0);
    t2 = ((char*)((ng7)));
    t3 = (t0 + 4296);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 14);
    xsi_set_current_line(95, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5416);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(97, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 7176);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    xsi_set_current_line(98, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5896);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(99, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 4936);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    goto LAB20;

LAB24:    xsi_set_current_line(106, ng0);

LAB41:    xsi_set_current_line(108, ng0);
    t7 = ((char*)((ng3)));
    t8 = (t0 + 5896);
    xsi_vlogvar_assign_value(t8, t7, 0, 0, 1);
    xsi_set_current_line(109, ng0);
    t2 = ((char*)((ng7)));
    t3 = (t0 + 6216);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    goto LAB40;

LAB26:    xsi_set_current_line(113, ng0);

LAB42:    xsi_set_current_line(115, ng0);
    t3 = (t0 + 6216);
    t5 = (t3 + 56U);
    t7 = *((char **)t5);
    t8 = ((char*)((ng9)));
    memset(t6, 0, 8);
    t21 = (t7 + 4);
    if (*((unsigned int *)t21) != 0)
        goto LAB44;

LAB43:    t22 = (t8 + 4);
    if (*((unsigned int *)t22) != 0)
        goto LAB44;

LAB47:    if (*((unsigned int *)t7) < *((unsigned int *)t8))
        goto LAB45;

LAB46:    t29 = (t6 + 4);
    t9 = *((unsigned int *)t29);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB48;

LAB49:
LAB50:    xsi_set_current_line(128, ng0);
    t2 = (t0 + 6216);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng2)));
    memset(t6, 0, 8);
    t8 = (t5 + 4);
    t21 = (t7 + 4);
    t9 = *((unsigned int *)t5);
    t10 = *((unsigned int *)t7);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t8);
    t13 = *((unsigned int *)t21);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t8);
    t17 = *((unsigned int *)t21);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB61;

LAB60:    if (t18 != 0)
        goto LAB62;

LAB63:    memset(t35, 0, 8);
    t28 = (t6 + 4);
    t23 = *((unsigned int *)t28);
    t24 = (~(t23));
    t25 = *((unsigned int *)t6);
    t26 = (t25 & t24);
    t27 = (t26 & 1U);
    if (t27 != 0)
        goto LAB64;

LAB65:    if (*((unsigned int *)t28) != 0)
        goto LAB66;

LAB67:    t31 = (t35 + 4);
    t40 = *((unsigned int *)t35);
    t41 = *((unsigned int *)t31);
    t42 = (t40 || t41);
    if (t42 > 0)
        goto LAB68;

LAB69:    memcpy(t52, t35, 8);

LAB70:    t83 = (t52 + 4);
    t84 = *((unsigned int *)t83);
    t85 = (~(t84));
    t86 = *((unsigned int *)t52);
    t87 = (t86 & t85);
    t88 = (t87 != 0);
    if (t88 > 0)
        goto LAB83;

LAB84:
LAB85:    xsi_set_current_line(131, ng0);
    t2 = (t0 + 7176);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng2)));
    memset(t6, 0, 8);
    t8 = (t5 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB93;

LAB92:    t21 = (t7 + 4);
    if (*((unsigned int *)t21) != 0)
        goto LAB93;

LAB96:    if (*((unsigned int *)t5) > *((unsigned int *)t7))
        goto LAB94;

LAB95:    memset(t35, 0, 8);
    t28 = (t6 + 4);
    t9 = *((unsigned int *)t28);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 & 1U);
    if (t13 != 0)
        goto LAB97;

LAB98:    if (*((unsigned int *)t28) != 0)
        goto LAB99;

LAB100:    t31 = (t35 + 4);
    t14 = *((unsigned int *)t35);
    t15 = *((unsigned int *)t31);
    t16 = (t14 || t15);
    if (t16 > 0)
        goto LAB101;

LAB102:    memcpy(t52, t35, 8);

LAB103:    t83 = (t52 + 4);
    t70 = *((unsigned int *)t83);
    t71 = (~(t70));
    t72 = *((unsigned int *)t52);
    t73 = (t72 & t71);
    t74 = (t73 != 0);
    if (t74 > 0)
        goto LAB116;

LAB117:    xsi_set_current_line(142, ng0);
    t2 = (t0 + 6216);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng9)));
    memset(t6, 0, 8);
    t8 = (t5 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB148;

LAB147:    t21 = (t7 + 4);
    if (*((unsigned int *)t21) != 0)
        goto LAB148;

LAB151:    if (*((unsigned int *)t5) > *((unsigned int *)t7))
        goto LAB149;

LAB150:    t28 = (t6 + 4);
    t9 = *((unsigned int *)t28);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB152;

LAB153:
LAB154:
LAB118:    goto LAB40;

LAB28:    xsi_set_current_line(153, ng0);

LAB156:    xsi_set_current_line(155, ng0);
    t3 = ((char*)((ng3)));
    t5 = (t0 + 5736);
    xsi_vlogvar_assign_value(t5, t3, 0, 0, 1);
    xsi_set_current_line(156, ng0);
    t2 = ((char*)((ng7)));
    t3 = (t0 + 6216);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    xsi_set_current_line(157, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5416);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(158, ng0);
    t2 = ((char*)((ng7)));
    t3 = (t0 + 4296);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 14);
    xsi_set_current_line(159, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 6856);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    goto LAB40;

LAB30:    xsi_set_current_line(162, ng0);

LAB157:    xsi_set_current_line(163, ng0);
    t3 = (t0 + 6216);
    t5 = (t3 + 56U);
    t7 = *((char **)t5);
    t8 = ((char*)((ng2)));
    memset(t6, 0, 8);
    t21 = (t7 + 4);
    t22 = (t8 + 4);
    t9 = *((unsigned int *)t7);
    t10 = *((unsigned int *)t8);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t21);
    t13 = *((unsigned int *)t22);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t21);
    t17 = *((unsigned int *)t22);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB161;

LAB158:    if (t18 != 0)
        goto LAB160;

LAB159:    *((unsigned int *)t6) = 1;

LAB161:    t29 = (t6 + 4);
    t23 = *((unsigned int *)t29);
    t24 = (~(t23));
    t25 = *((unsigned int *)t6);
    t26 = (t25 & t24);
    t27 = (t26 != 0);
    if (t27 > 0)
        goto LAB162;

LAB163:    xsi_set_current_line(169, ng0);
    t2 = (t0 + 6216);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng12)));
    memset(t6, 0, 8);
    t8 = (t5 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB167;

LAB166:    t21 = (t7 + 4);
    if (*((unsigned int *)t21) != 0)
        goto LAB167;

LAB170:    if (*((unsigned int *)t5) < *((unsigned int *)t7))
        goto LAB168;

LAB169:    t28 = (t6 + 4);
    t9 = *((unsigned int *)t28);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB171;

LAB172:
LAB173:
LAB164:    goto LAB40;

LAB32:    xsi_set_current_line(180, ng0);

LAB177:    xsi_set_current_line(182, ng0);
    t3 = (t0 + 6216);
    t5 = (t3 + 56U);
    t7 = *((char **)t5);
    t8 = ((char*)((ng12)));
    memset(t6, 0, 8);
    t21 = (t7 + 4);
    if (*((unsigned int *)t21) != 0)
        goto LAB179;

LAB178:    t22 = (t8 + 4);
    if (*((unsigned int *)t22) != 0)
        goto LAB179;

LAB182:    if (*((unsigned int *)t7) < *((unsigned int *)t8))
        goto LAB180;

LAB181:    t29 = (t6 + 4);
    t9 = *((unsigned int *)t29);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB183;

LAB184:    xsi_set_current_line(190, ng0);
    t2 = (t0 + 6216);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng15)));
    memset(t6, 0, 8);
    t8 = (t5 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB188;

LAB187:    t21 = (t7 + 4);
    if (*((unsigned int *)t21) != 0)
        goto LAB188;

LAB191:    if (*((unsigned int *)t5) < *((unsigned int *)t7))
        goto LAB189;

LAB190:    t28 = (t6 + 4);
    t9 = *((unsigned int *)t28);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB192;

LAB193:    xsi_set_current_line(196, ng0);

LAB196:    xsi_set_current_line(197, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 4136);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(199, ng0);
    t2 = ((char*)((ng16)));
    t3 = (t0 + 6856);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(200, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 6376);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(201, ng0);
    t2 = ((char*)((ng7)));
    t3 = (t0 + 6216);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    xsi_set_current_line(202, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5416);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(203, ng0);
    t2 = (t0 + 7016);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    memset(t6, 0, 8);
    t7 = (t6 + 4);
    t8 = (t5 + 4);
    t9 = *((unsigned int *)t5);
    t10 = (t9 >> 18);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t8);
    t12 = (t11 >> 18);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t13 & 16383U);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 & 16383U);
    t21 = (t0 + 4296);
    xsi_vlogvar_assign_value(t21, t6, 0, 0, 14);
    xsi_set_current_line(204, ng0);
    t2 = (t0 + 4296);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    memset(t6, 0, 8);
    t7 = (t6 + 4);
    t8 = (t5 + 4);
    t9 = *((unsigned int *)t5);
    t10 = (t9 >> 2);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t8);
    t12 = (t11 >> 2);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t13 & 4095U);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 & 4095U);
    t21 = (t0 + 7336);
    t22 = (t0 + 7336);
    t28 = (t22 + 72U);
    t29 = *((char **)t28);
    t31 = ((char*)((ng17)));
    t32 = ((char*)((ng18)));
    xsi_vlog_convert_partindices(t35, t47, t48, ((int*)(t29)), 2, t31, 32, 1, t32, 32, 1);
    t33 = (t35 + 4);
    t15 = *((unsigned int *)t33);
    t30 = (!(t15));
    t34 = (t47 + 4);
    t16 = *((unsigned int *)t34);
    t76 = (!(t16));
    t149 = (t30 && t76);
    t36 = (t48 + 4);
    t17 = *((unsigned int *)t36);
    t150 = (!(t17));
    t174 = (t149 && t150);
    if (t174 == 1)
        goto LAB197;

LAB198:
LAB194:
LAB185:    goto LAB40;

LAB34:    xsi_set_current_line(209, ng0);

LAB199:    xsi_set_current_line(210, ng0);
    t3 = ((char*)((ng2)));
    t5 = (t0 + 5736);
    xsi_vlogvar_assign_value(t5, t3, 0, 0, 1);
    xsi_set_current_line(211, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 4936);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(212, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 6376);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(213, ng0);
    t2 = ((char*)((ng7)));
    t3 = (t0 + 6216);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    xsi_set_current_line(214, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5416);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(215, ng0);
    t2 = ((char*)((ng5)));
    t3 = (t0 + 6856);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(216, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 4456);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(217, ng0);
    t2 = ((char*)((ng19)));
    t3 = (t0 + 7336);
    t5 = (t0 + 7336);
    t7 = (t5 + 72U);
    t8 = *((char **)t7);
    t21 = ((char*)((ng20)));
    t22 = ((char*)((ng21)));
    xsi_vlog_convert_partindices(t6, t35, t47, ((int*)(t8)), 2, t21, 32, 1, t22, 32, 1);
    t28 = (t6 + 4);
    t9 = *((unsigned int *)t28);
    t30 = (!(t9));
    t29 = (t35 + 4);
    t10 = *((unsigned int *)t29);
    t76 = (!(t10));
    t149 = (t30 && t76);
    t31 = (t47 + 4);
    t11 = *((unsigned int *)t31);
    t150 = (!(t11));
    t174 = (t149 && t150);
    if (t174 == 1)
        goto LAB200;

LAB201:    goto LAB40;

LAB36:    xsi_set_current_line(220, ng0);

LAB202:    xsi_set_current_line(221, ng0);
    t3 = (t0 + 6216);
    t5 = (t3 + 56U);
    t7 = *((char **)t5);
    t8 = ((char*)((ng2)));
    memset(t6, 0, 8);
    t21 = (t7 + 4);
    t22 = (t8 + 4);
    t9 = *((unsigned int *)t7);
    t10 = *((unsigned int *)t8);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t21);
    t13 = *((unsigned int *)t22);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t21);
    t17 = *((unsigned int *)t22);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB206;

LAB203:    if (t18 != 0)
        goto LAB205;

LAB204:    *((unsigned int *)t6) = 1;

LAB206:    t29 = (t6 + 4);
    t23 = *((unsigned int *)t29);
    t24 = (~(t23));
    t25 = *((unsigned int *)t6);
    t26 = (t25 & t24);
    t27 = (t26 != 0);
    if (t27 > 0)
        goto LAB207;

LAB208:    xsi_set_current_line(229, ng0);
    t2 = (t0 + 6216);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng22)));
    memset(t6, 0, 8);
    t8 = (t5 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB212;

LAB211:    t21 = (t7 + 4);
    if (*((unsigned int *)t21) != 0)
        goto LAB212;

LAB215:    if (*((unsigned int *)t5) < *((unsigned int *)t7))
        goto LAB213;

LAB214:    t28 = (t6 + 4);
    t9 = *((unsigned int *)t28);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB216;

LAB217:
LAB218:
LAB209:    goto LAB40;

LAB38:    xsi_set_current_line(239, ng0);

LAB222:    xsi_set_current_line(241, ng0);
    t3 = (t0 + 6216);
    t5 = (t3 + 56U);
    t7 = *((char **)t5);
    t8 = ((char*)((ng22)));
    memset(t6, 0, 8);
    t21 = (t7 + 4);
    if (*((unsigned int *)t21) != 0)
        goto LAB224;

LAB223:    t22 = (t8 + 4);
    if (*((unsigned int *)t22) != 0)
        goto LAB224;

LAB227:    if (*((unsigned int *)t7) < *((unsigned int *)t8))
        goto LAB225;

LAB226:    t29 = (t6 + 4);
    t9 = *((unsigned int *)t29);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB228;

LAB229:    xsi_set_current_line(248, ng0);
    t2 = (t0 + 6216);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng24)));
    memset(t6, 0, 8);
    t8 = (t5 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB233;

LAB232:    t21 = (t7 + 4);
    if (*((unsigned int *)t21) != 0)
        goto LAB233;

LAB236:    if (*((unsigned int *)t5) < *((unsigned int *)t7))
        goto LAB234;

LAB235:    t28 = (t6 + 4);
    t9 = *((unsigned int *)t28);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB237;

LAB238:    xsi_set_current_line(254, ng0);

LAB241:    xsi_set_current_line(255, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 4136);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(256, ng0);
    t2 = ((char*)((ng8)));
    t3 = (t0 + 6856);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(257, ng0);
    t2 = ((char*)((ng7)));
    t3 = (t0 + 6216);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    xsi_set_current_line(258, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5416);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(260, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 7176);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    xsi_set_current_line(261, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 4936);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(262, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5896);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);

LAB239:
LAB230:    goto LAB40;

LAB44:    t28 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t28) = 1;
    goto LAB46;

LAB45:    *((unsigned int *)t6) = 1;
    goto LAB46;

LAB48:    xsi_set_current_line(116, ng0);

LAB51:    xsi_set_current_line(117, ng0);
    t31 = (t0 + 6216);
    t32 = (t31 + 56U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng2)));
    memset(t35, 0, 8);
    t36 = (t33 + 4);
    t37 = (t34 + 4);
    t14 = *((unsigned int *)t33);
    t15 = *((unsigned int *)t34);
    t16 = (t14 ^ t15);
    t17 = *((unsigned int *)t36);
    t18 = *((unsigned int *)t37);
    t19 = (t17 ^ t18);
    t20 = (t16 | t19);
    t23 = *((unsigned int *)t36);
    t24 = *((unsigned int *)t37);
    t25 = (t23 | t24);
    t26 = (~(t25));
    t27 = (t20 & t26);
    if (t27 != 0)
        goto LAB55;

LAB52:    if (t25 != 0)
        goto LAB54;

LAB53:    *((unsigned int *)t35) = 1;

LAB55:    t39 = (t35 + 4);
    t40 = *((unsigned int *)t39);
    t41 = (~(t40));
    t42 = *((unsigned int *)t35);
    t43 = (t42 & t41);
    t44 = (t43 != 0);
    if (t44 > 0)
        goto LAB56;

LAB57:
LAB58:    xsi_set_current_line(124, ng0);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 7176);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    goto LAB50;

LAB54:    t38 = (t35 + 4);
    *((unsigned int *)t35) = 1;
    *((unsigned int *)t38) = 1;
    goto LAB55;

LAB56:    xsi_set_current_line(118, ng0);

LAB59:    xsi_set_current_line(119, ng0);
    t45 = ((char*)((ng2)));
    t46 = (t0 + 6696);
    xsi_vlogvar_assign_value(t46, t45, 0, 0, 4);
    xsi_set_current_line(120, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 4616);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(121, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5416);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    goto LAB58;

LAB61:    *((unsigned int *)t6) = 1;
    goto LAB63;

LAB62:    t22 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB63;

LAB64:    *((unsigned int *)t35) = 1;
    goto LAB67;

LAB66:    t29 = (t35 + 4);
    *((unsigned int *)t35) = 1;
    *((unsigned int *)t29) = 1;
    goto LAB67;

LAB68:    t32 = (t0 + 7176);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    t36 = ((char*)((ng2)));
    memset(t47, 0, 8);
    t37 = (t34 + 4);
    if (*((unsigned int *)t37) != 0)
        goto LAB72;

LAB71:    t38 = (t36 + 4);
    if (*((unsigned int *)t38) != 0)
        goto LAB72;

LAB75:    if (*((unsigned int *)t34) > *((unsigned int *)t36))
        goto LAB73;

LAB74:    memset(t48, 0, 8);
    t45 = (t47 + 4);
    t43 = *((unsigned int *)t45);
    t44 = (~(t43));
    t49 = *((unsigned int *)t47);
    t50 = (t49 & t44);
    t51 = (t50 & 1U);
    if (t51 != 0)
        goto LAB76;

LAB77:    if (*((unsigned int *)t45) != 0)
        goto LAB78;

LAB79:    t53 = *((unsigned int *)t35);
    t54 = *((unsigned int *)t48);
    t55 = (t53 & t54);
    *((unsigned int *)t52) = t55;
    t56 = (t35 + 4);
    t57 = (t48 + 4);
    t58 = (t52 + 4);
    t59 = *((unsigned int *)t56);
    t60 = *((unsigned int *)t57);
    t61 = (t59 | t60);
    *((unsigned int *)t58) = t61;
    t62 = *((unsigned int *)t58);
    t63 = (t62 != 0);
    if (t63 == 1)
        goto LAB80;

LAB81:
LAB82:    goto LAB70;

LAB72:    t39 = (t47 + 4);
    *((unsigned int *)t47) = 1;
    *((unsigned int *)t39) = 1;
    goto LAB74;

LAB73:    *((unsigned int *)t47) = 1;
    goto LAB74;

LAB76:    *((unsigned int *)t48) = 1;
    goto LAB79;

LAB78:    t46 = (t48 + 4);
    *((unsigned int *)t48) = 1;
    *((unsigned int *)t46) = 1;
    goto LAB79;

LAB80:    t64 = *((unsigned int *)t52);
    t65 = *((unsigned int *)t58);
    *((unsigned int *)t52) = (t64 | t65);
    t66 = (t35 + 4);
    t67 = (t48 + 4);
    t68 = *((unsigned int *)t35);
    t69 = (~(t68));
    t70 = *((unsigned int *)t66);
    t71 = (~(t70));
    t72 = *((unsigned int *)t48);
    t73 = (~(t72));
    t74 = *((unsigned int *)t67);
    t75 = (~(t74));
    t30 = (t69 & t71);
    t76 = (t73 & t75);
    t77 = (~(t30));
    t78 = (~(t76));
    t79 = *((unsigned int *)t58);
    *((unsigned int *)t58) = (t79 & t77);
    t80 = *((unsigned int *)t58);
    *((unsigned int *)t58) = (t80 & t78);
    t81 = *((unsigned int *)t52);
    *((unsigned int *)t52) = (t81 & t77);
    t82 = *((unsigned int *)t52);
    *((unsigned int *)t52) = (t82 & t78);
    goto LAB82;

LAB83:    xsi_set_current_line(129, ng0);
    t90 = (t0 + 5416);
    t91 = (t90 + 56U);
    t92 = *((char **)t91);
    memset(t89, 0, 8);
    t93 = (t92 + 4);
    t94 = *((unsigned int *)t93);
    t95 = (~(t94));
    t96 = *((unsigned int *)t92);
    t97 = (t96 & t95);
    t98 = (t97 & 1U);
    if (t98 != 0)
        goto LAB89;

LAB87:    if (*((unsigned int *)t93) == 0)
        goto LAB86;

LAB88:    t99 = (t89 + 4);
    *((unsigned int *)t89) = 1;
    *((unsigned int *)t99) = 1;

LAB89:    t100 = (t89 + 4);
    t101 = (t92 + 4);
    t102 = *((unsigned int *)t92);
    t103 = (~(t102));
    *((unsigned int *)t89) = t103;
    *((unsigned int *)t100) = 0;
    if (*((unsigned int *)t101) != 0)
        goto LAB91;

LAB90:    t108 = *((unsigned int *)t89);
    *((unsigned int *)t89) = (t108 & 1U);
    t109 = *((unsigned int *)t100);
    *((unsigned int *)t100) = (t109 & 1U);
    t110 = (t0 + 5416);
    xsi_vlogvar_assign_value(t110, t89, 0, 0, 1);
    goto LAB85;

LAB86:    *((unsigned int *)t89) = 1;
    goto LAB89;

LAB91:    t104 = *((unsigned int *)t89);
    t105 = *((unsigned int *)t101);
    *((unsigned int *)t89) = (t104 | t105);
    t106 = *((unsigned int *)t100);
    t107 = *((unsigned int *)t101);
    *((unsigned int *)t100) = (t106 | t107);
    goto LAB90;

LAB93:    t22 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB95;

LAB94:    *((unsigned int *)t6) = 1;
    goto LAB95;

LAB97:    *((unsigned int *)t35) = 1;
    goto LAB100;

LAB99:    t29 = (t35 + 4);
    *((unsigned int *)t35) = 1;
    *((unsigned int *)t29) = 1;
    goto LAB100;

LAB101:    t32 = (t0 + 6216);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    t36 = ((char*)((ng10)));
    memset(t47, 0, 8);
    t37 = (t34 + 4);
    if (*((unsigned int *)t37) != 0)
        goto LAB105;

LAB104:    t38 = (t36 + 4);
    if (*((unsigned int *)t38) != 0)
        goto LAB105;

LAB108:    if (*((unsigned int *)t34) < *((unsigned int *)t36))
        goto LAB106;

LAB107:    memset(t48, 0, 8);
    t45 = (t47 + 4);
    t17 = *((unsigned int *)t45);
    t18 = (~(t17));
    t19 = *((unsigned int *)t47);
    t20 = (t19 & t18);
    t23 = (t20 & 1U);
    if (t23 != 0)
        goto LAB109;

LAB110:    if (*((unsigned int *)t45) != 0)
        goto LAB111;

LAB112:    t24 = *((unsigned int *)t35);
    t25 = *((unsigned int *)t48);
    t26 = (t24 & t25);
    *((unsigned int *)t52) = t26;
    t56 = (t35 + 4);
    t57 = (t48 + 4);
    t58 = (t52 + 4);
    t27 = *((unsigned int *)t56);
    t40 = *((unsigned int *)t57);
    t41 = (t27 | t40);
    *((unsigned int *)t58) = t41;
    t42 = *((unsigned int *)t58);
    t43 = (t42 != 0);
    if (t43 == 1)
        goto LAB113;

LAB114:
LAB115:    goto LAB103;

LAB105:    t39 = (t47 + 4);
    *((unsigned int *)t47) = 1;
    *((unsigned int *)t39) = 1;
    goto LAB107;

LAB106:    *((unsigned int *)t47) = 1;
    goto LAB107;

LAB109:    *((unsigned int *)t48) = 1;
    goto LAB112;

LAB111:    t46 = (t48 + 4);
    *((unsigned int *)t48) = 1;
    *((unsigned int *)t46) = 1;
    goto LAB112;

LAB113:    t44 = *((unsigned int *)t52);
    t49 = *((unsigned int *)t58);
    *((unsigned int *)t52) = (t44 | t49);
    t66 = (t35 + 4);
    t67 = (t48 + 4);
    t50 = *((unsigned int *)t35);
    t51 = (~(t50));
    t53 = *((unsigned int *)t66);
    t54 = (~(t53));
    t55 = *((unsigned int *)t48);
    t59 = (~(t55));
    t60 = *((unsigned int *)t67);
    t61 = (~(t60));
    t30 = (t51 & t54);
    t76 = (t59 & t61);
    t62 = (~(t30));
    t63 = (~(t76));
    t64 = *((unsigned int *)t58);
    *((unsigned int *)t58) = (t64 & t62);
    t65 = *((unsigned int *)t58);
    *((unsigned int *)t58) = (t65 & t63);
    t68 = *((unsigned int *)t52);
    *((unsigned int *)t52) = (t68 & t62);
    t69 = *((unsigned int *)t52);
    *((unsigned int *)t52) = (t69 & t63);
    goto LAB115;

LAB116:    xsi_set_current_line(132, ng0);

LAB119:    xsi_set_current_line(133, ng0);
    t90 = (t0 + 6216);
    t91 = (t90 + 56U);
    t92 = *((char **)t91);
    t93 = ((char*)((ng9)));
    memset(t89, 0, 8);
    t99 = (t92 + 4);
    if (*((unsigned int *)t99) != 0)
        goto LAB121;

LAB120:    t100 = (t93 + 4);
    if (*((unsigned int *)t100) != 0)
        goto LAB121;

LAB124:    if (*((unsigned int *)t92) < *((unsigned int *)t93))
        goto LAB122;

LAB123:    memset(t111, 0, 8);
    t110 = (t89 + 4);
    t75 = *((unsigned int *)t110);
    t77 = (~(t75));
    t78 = *((unsigned int *)t89);
    t79 = (t78 & t77);
    t80 = (t79 & 1U);
    if (t80 != 0)
        goto LAB125;

LAB126:    if (*((unsigned int *)t110) != 0)
        goto LAB127;

LAB128:    t113 = (t111 + 4);
    t81 = *((unsigned int *)t111);
    t82 = *((unsigned int *)t113);
    t84 = (t81 || t82);
    if (t84 > 0)
        goto LAB129;

LAB130:    memcpy(t125, t111, 8);

LAB131:    t157 = (t125 + 4);
    t158 = *((unsigned int *)t157);
    t159 = (~(t158));
    t160 = *((unsigned int *)t125);
    t161 = (t160 & t159);
    t162 = (t161 != 0);
    if (t162 > 0)
        goto LAB143;

LAB144:
LAB145:    xsi_set_current_line(138, ng0);
    t2 = (t0 + 6216);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng3)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 32, t5, 8, t7, 32);
    t8 = (t0 + 6216);
    xsi_vlogvar_assign_value(t8, t6, 0, 0, 8);
    goto LAB118;

LAB121:    t101 = (t89 + 4);
    *((unsigned int *)t89) = 1;
    *((unsigned int *)t101) = 1;
    goto LAB123;

LAB122:    *((unsigned int *)t89) = 1;
    goto LAB123;

LAB125:    *((unsigned int *)t111) = 1;
    goto LAB128;

LAB127:    t112 = (t111 + 4);
    *((unsigned int *)t111) = 1;
    *((unsigned int *)t112) = 1;
    goto LAB128;

LAB129:    t114 = (t0 + 5416);
    t115 = (t114 + 56U);
    t116 = *((char **)t115);
    t117 = ((char*)((ng2)));
    memset(t118, 0, 8);
    t119 = (t116 + 4);
    t120 = (t117 + 4);
    t85 = *((unsigned int *)t116);
    t86 = *((unsigned int *)t117);
    t87 = (t85 ^ t86);
    t88 = *((unsigned int *)t119);
    t94 = *((unsigned int *)t120);
    t95 = (t88 ^ t94);
    t96 = (t87 | t95);
    t97 = *((unsigned int *)t119);
    t98 = *((unsigned int *)t120);
    t102 = (t97 | t98);
    t103 = (~(t102));
    t104 = (t96 & t103);
    if (t104 != 0)
        goto LAB135;

LAB132:    if (t102 != 0)
        goto LAB134;

LAB133:    *((unsigned int *)t118) = 1;

LAB135:    memset(t122, 0, 8);
    t123 = (t118 + 4);
    t105 = *((unsigned int *)t123);
    t106 = (~(t105));
    t107 = *((unsigned int *)t118);
    t108 = (t107 & t106);
    t109 = (t108 & 1U);
    if (t109 != 0)
        goto LAB136;

LAB137:    if (*((unsigned int *)t123) != 0)
        goto LAB138;

LAB139:    t126 = *((unsigned int *)t111);
    t127 = *((unsigned int *)t122);
    t128 = (t126 & t127);
    *((unsigned int *)t125) = t128;
    t129 = (t111 + 4);
    t130 = (t122 + 4);
    t131 = (t125 + 4);
    t132 = *((unsigned int *)t129);
    t133 = *((unsigned int *)t130);
    t134 = (t132 | t133);
    *((unsigned int *)t131) = t134;
    t135 = *((unsigned int *)t131);
    t136 = (t135 != 0);
    if (t136 == 1)
        goto LAB140;

LAB141:
LAB142:    goto LAB131;

LAB134:    t121 = (t118 + 4);
    *((unsigned int *)t118) = 1;
    *((unsigned int *)t121) = 1;
    goto LAB135;

LAB136:    *((unsigned int *)t122) = 1;
    goto LAB139;

LAB138:    t124 = (t122 + 4);
    *((unsigned int *)t122) = 1;
    *((unsigned int *)t124) = 1;
    goto LAB139;

LAB140:    t137 = *((unsigned int *)t125);
    t138 = *((unsigned int *)t131);
    *((unsigned int *)t125) = (t137 | t138);
    t139 = (t111 + 4);
    t140 = (t122 + 4);
    t141 = *((unsigned int *)t111);
    t142 = (~(t141));
    t143 = *((unsigned int *)t139);
    t144 = (~(t143));
    t145 = *((unsigned int *)t122);
    t146 = (~(t145));
    t147 = *((unsigned int *)t140);
    t148 = (~(t147));
    t149 = (t142 & t144);
    t150 = (t146 & t148);
    t151 = (~(t149));
    t152 = (~(t150));
    t153 = *((unsigned int *)t131);
    *((unsigned int *)t131) = (t153 & t151);
    t154 = *((unsigned int *)t131);
    *((unsigned int *)t131) = (t154 & t152);
    t155 = *((unsigned int *)t125);
    *((unsigned int *)t125) = (t155 & t151);
    t156 = *((unsigned int *)t125);
    *((unsigned int *)t125) = (t156 & t152);
    goto LAB142;

LAB143:    xsi_set_current_line(134, ng0);

LAB146:    xsi_set_current_line(135, ng0);
    t163 = (t0 + 2776U);
    t164 = *((char **)t163);
    t163 = (t0 + 2736U);
    t166 = (t163 + 72U);
    t167 = *((char **)t166);
    t168 = ((char*)((ng11)));
    t169 = (t0 + 6696);
    t170 = (t169 + 56U);
    t171 = *((char **)t170);
    memset(t172, 0, 8);
    xsi_vlog_unsigned_minus(t172, 32, t168, 32, t171, 4);
    xsi_vlog_generic_get_index_select_value(t165, 1, t164, t167, 2, t172, 32, 2);
    t173 = (t0 + 4456);
    xsi_vlogvar_assign_value(t173, t165, 0, 0, 1);
    xsi_set_current_line(136, ng0);
    t2 = (t0 + 6696);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng3)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 32, t5, 4, t7, 32);
    t8 = (t0 + 6696);
    xsi_vlogvar_assign_value(t8, t6, 0, 0, 4);
    goto LAB145;

LAB148:    t22 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB150;

LAB149:    *((unsigned int *)t6) = 1;
    goto LAB150;

LAB152:    xsi_set_current_line(143, ng0);

LAB155:    xsi_set_current_line(144, ng0);
    t29 = ((char*)((ng7)));
    t31 = (t0 + 6856);
    xsi_vlogvar_wait_assign_value(t31, t29, 0, 0, 3, 0LL);
    xsi_set_current_line(145, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 4616);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(146, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 6216);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    xsi_set_current_line(147, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 7176);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    xsi_set_current_line(148, ng0);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 4136);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(149, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5416);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    goto LAB154;

LAB160:    t28 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t28) = 1;
    goto LAB161;

LAB162:    xsi_set_current_line(164, ng0);

LAB165:    xsi_set_current_line(165, ng0);
    t31 = ((char*)((ng2)));
    t32 = (t0 + 5736);
    xsi_vlogvar_assign_value(t32, t31, 0, 0, 1);
    xsi_set_current_line(166, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 6216);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    goto LAB164;

LAB167:    t22 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB169;

LAB168:    *((unsigned int *)t6) = 1;
    goto LAB169;

LAB171:    xsi_set_current_line(170, ng0);

LAB174:    xsi_set_current_line(171, ng0);
    t29 = ((char*)((ng2)));
    t31 = (t0 + 4136);
    xsi_vlogvar_assign_value(t31, t29, 0, 0, 1);
    xsi_set_current_line(172, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 5416);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(173, ng0);
    t2 = ((char*)((ng13)));
    t3 = (t0 + 6856);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(174, ng0);
    t2 = (t0 + 3096U);
    t3 = *((char **)t2);
    t2 = (t0 + 7016);
    t5 = (t0 + 7016);
    t7 = (t5 + 72U);
    t8 = *((char **)t7);
    t21 = ((char*)((ng14)));
    t22 = (t0 + 6216);
    t28 = (t22 + 56U);
    t29 = *((char **)t28);
    t31 = ((char*)((ng3)));
    memset(t35, 0, 8);
    xsi_vlog_unsigned_minus(t35, 32, t29, 8, t31, 32);
    memset(t47, 0, 8);
    xsi_vlog_unsigned_minus(t47, 32, t21, 32, t35, 32);
    xsi_vlog_generic_convert_bit_index(t6, t8, 2, t47, 32, 2);
    t32 = (t6 + 4);
    t9 = *((unsigned int *)t32);
    t30 = (!(t9));
    if (t30 == 1)
        goto LAB175;

LAB176:    xsi_set_current_line(175, ng0);
    t2 = (t0 + 7016);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    memset(t6, 0, 8);
    t7 = (t6 + 4);
    t8 = (t5 + 4);
    t9 = *((unsigned int *)t5);
    t10 = (t9 >> 18);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t8);
    t12 = (t11 >> 18);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t13 & 16383U);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 & 16383U);
    t21 = (t0 + 4296);
    xsi_vlogvar_assign_value(t21, t6, 0, 0, 14);
    xsi_set_current_line(176, ng0);
    t2 = (t0 + 6216);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng3)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 32, t5, 8, t7, 32);
    t8 = (t0 + 6216);
    xsi_vlogvar_assign_value(t8, t6, 0, 0, 8);
    goto LAB173;

LAB175:    xsi_vlogvar_assign_value(t2, t3, 0, *((unsigned int *)t6), 1);
    goto LAB176;

LAB179:    t28 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t28) = 1;
    goto LAB181;

LAB180:    *((unsigned int *)t6) = 1;
    goto LAB181;

LAB183:    xsi_set_current_line(183, ng0);

LAB186:    xsi_set_current_line(184, ng0);
    t31 = ((char*)((ng2)));
    t32 = (t0 + 4136);
    xsi_vlogvar_assign_value(t32, t31, 0, 0, 1);
    xsi_set_current_line(185, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5416);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(186, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 6856);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    goto LAB185;

LAB188:    t22 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB190;

LAB189:    *((unsigned int *)t6) = 1;
    goto LAB190;

LAB192:    xsi_set_current_line(191, ng0);

LAB195:    xsi_set_current_line(192, ng0);
    t29 = ((char*)((ng2)));
    t31 = (t0 + 5416);
    xsi_vlogvar_assign_value(t31, t29, 0, 0, 1);
    xsi_set_current_line(193, ng0);
    t2 = (t0 + 6216);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng1)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 8, t5, 8, t7, 8);
    t8 = (t0 + 6216);
    xsi_vlogvar_assign_value(t8, t6, 0, 0, 8);
    goto LAB194;

LAB197:    t18 = *((unsigned int *)t48);
    t175 = (t18 + 0);
    t19 = *((unsigned int *)t35);
    t20 = *((unsigned int *)t47);
    t176 = (t19 - t20);
    t177 = (t176 + 1);
    xsi_vlogvar_assign_value(t21, t6, t175, *((unsigned int *)t47), t177);
    goto LAB198;

LAB200:    t12 = *((unsigned int *)t47);
    t175 = (t12 + 0);
    t13 = *((unsigned int *)t6);
    t14 = *((unsigned int *)t35);
    t176 = (t13 - t14);
    t177 = (t176 + 1);
    xsi_vlogvar_assign_value(t3, t2, t175, *((unsigned int *)t35), t177);
    goto LAB201;

LAB205:    t28 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t28) = 1;
    goto LAB206;

LAB207:    xsi_set_current_line(222, ng0);

LAB210:    xsi_set_current_line(224, ng0);
    t31 = ((char*)((ng3)));
    t32 = (t0 + 6376);
    xsi_vlogvar_assign_value(t32, t31, 0, 0, 1);
    xsi_set_current_line(225, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 6216);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 8);
    xsi_set_current_line(226, ng0);
    t2 = ((char*)((ng6)));
    t3 = (t0 + 6856);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    goto LAB209;

LAB212:    t22 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB214;

LAB213:    *((unsigned int *)t6) = 1;
    goto LAB214;

LAB216:    xsi_set_current_line(230, ng0);

LAB219:    xsi_set_current_line(231, ng0);
    t29 = ((char*)((ng2)));
    t31 = (t0 + 4136);
    xsi_vlogvar_assign_value(t31, t29, 0, 0, 1);
    xsi_set_current_line(232, ng0);
    t2 = (t0 + 3096U);
    t3 = *((char **)t2);
    t2 = (t0 + 6536);
    t5 = (t0 + 6536);
    t7 = (t5 + 72U);
    t8 = *((char **)t7);
    t21 = ((char*)((ng23)));
    t22 = (t0 + 6216);
    t28 = (t22 + 56U);
    t29 = *((char **)t28);
    memset(t35, 0, 8);
    xsi_vlog_unsigned_minus(t35, 32, t21, 32, t29, 8);
    xsi_vlog_generic_convert_bit_index(t6, t8, 2, t35, 32, 2);
    t31 = (t6 + 4);
    t9 = *((unsigned int *)t31);
    t30 = (!(t9));
    if (t30 == 1)
        goto LAB220;

LAB221:    xsi_set_current_line(233, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 5416);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(234, ng0);
    t2 = ((char*)((ng6)));
    t3 = (t0 + 6856);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(235, ng0);
    t2 = (t0 + 6216);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng3)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 32, t5, 8, t7, 32);
    t8 = (t0 + 6216);
    xsi_vlogvar_assign_value(t8, t6, 0, 0, 8);
    goto LAB218;

LAB220:    xsi_vlogvar_assign_value(t2, t3, 0, *((unsigned int *)t6), 1);
    goto LAB221;

LAB224:    t28 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t28) = 1;
    goto LAB226;

LAB225:    *((unsigned int *)t6) = 1;
    goto LAB226;

LAB228:    xsi_set_current_line(242, ng0);

LAB231:    xsi_set_current_line(243, ng0);
    t31 = ((char*)((ng2)));
    t32 = (t0 + 4136);
    xsi_vlogvar_assign_value(t32, t31, 0, 0, 1);
    xsi_set_current_line(244, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 5416);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(245, ng0);
    t2 = ((char*)((ng5)));
    t3 = (t0 + 6856);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(246, ng0);
    t2 = (t0 + 7336);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = (t0 + 7336);
    t8 = (t7 + 72U);
    t21 = *((char **)t8);
    t22 = ((char*)((ng23)));
    t28 = (t0 + 6216);
    t29 = (t28 + 56U);
    t31 = *((char **)t29);
    memset(t35, 0, 8);
    xsi_vlog_unsigned_minus(t35, 32, t22, 32, t31, 8);
    xsi_vlog_generic_get_index_select_value(t6, 1, t5, t21, 2, t35, 32, 2);
    t32 = (t0 + 4456);
    xsi_vlogvar_assign_value(t32, t6, 0, 0, 1);
    goto LAB230;

LAB233:    t22 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB235;

LAB234:    *((unsigned int *)t6) = 1;
    goto LAB235;

LAB237:    xsi_set_current_line(249, ng0);

LAB240:    xsi_set_current_line(250, ng0);
    t29 = ((char*)((ng2)));
    t31 = (t0 + 5416);
    xsi_vlogvar_assign_value(t31, t29, 0, 0, 1);
    xsi_set_current_line(251, ng0);
    t2 = (t0 + 6216);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng1)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 8, t5, 8, t7, 8);
    t8 = (t0 + 6216);
    xsi_vlogvar_assign_value(t8, t6, 0, 0, 8);
    goto LAB239;

}

static void implSig1_execute(char *t0)
{
    char t3[8];
    char *t1;
    char *t2;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    unsigned int t12;
    unsigned int t13;
    char *t14;
    unsigned int t15;
    unsigned int t16;
    char *t17;
    unsigned int t18;
    unsigned int t19;
    char *t20;

LAB0:    t1 = (t0 + 8904U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    t2 = (t0 + 3096U);
    t4 = *((char **)t2);
    t2 = (t0 + 4456);
    t5 = (t2 + 56U);
    t6 = *((char **)t5);
    xsi_vlogtype_concat(t3, 2, 2, 2U, t6, 1, t4, 1);
    t7 = (t0 + 9864);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t10 = (t9 + 56U);
    t11 = *((char **)t10);
    memset(t11, 0, 8);
    t12 = 3U;
    t13 = t12;
    t14 = (t3 + 4);
    t15 = *((unsigned int *)t3);
    t12 = (t12 & t15);
    t16 = *((unsigned int *)t14);
    t13 = (t13 & t16);
    t17 = (t11 + 4);
    t18 = *((unsigned int *)t11);
    *((unsigned int *)t11) = (t18 | t12);
    t19 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t19 | t13);
    xsi_driver_vfirst_trans(t7, 0, 1);
    t20 = (t0 + 9752);
    *((int *)t20) = 1;

LAB1:    return;
}

static void implSig2_execute(char *t0)
{
    char t3[8];
    char *t1;
    char *t2;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    char *t25;
    unsigned int t26;
    unsigned int t27;
    char *t28;
    unsigned int t29;
    unsigned int t30;
    char *t31;

LAB0:    t1 = (t0 + 9152U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    t2 = (t0 + 5256);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t6 = (t0 + 5096);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t0 + 4936);
    t10 = (t9 + 56U);
    t11 = *((char **)t10);
    t12 = (t0 + 4776);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t15 = (t0 + 4616);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    xsi_vlogtype_concat(t3, 5, 5, 5U, t17, 1, t14, 1, t11, 1, t8, 1, t5, 1);
    t18 = (t0 + 9928);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    memset(t22, 0, 8);
    t23 = 31U;
    t24 = t23;
    t25 = (t3 + 4);
    t26 = *((unsigned int *)t3);
    t23 = (t23 & t26);
    t27 = *((unsigned int *)t25);
    t24 = (t24 & t27);
    t28 = (t22 + 4);
    t29 = *((unsigned int *)t22);
    *((unsigned int *)t22) = (t29 | t23);
    t30 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t30 | t24);
    xsi_driver_vfirst_trans(t18, 0, 4);
    t31 = (t0 + 9768);
    *((int *)t31) = 1;

LAB1:    return;
}

static void implSig3_execute(char *t0)
{
    char t3[8];
    char *t1;
    char *t2;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    char *t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;

LAB0:    t1 = (t0 + 9400U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    t2 = (t0 + 5736);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t6 = (t0 + 6056);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    xsi_vlogtype_concat(t3, 2, 2, 2U, t8, 1, t5, 1);
    t9 = (t0 + 9992);
    t10 = (t9 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memset(t13, 0, 8);
    t14 = 3U;
    t15 = t14;
    t16 = (t3 + 4);
    t17 = *((unsigned int *)t3);
    t14 = (t14 & t17);
    t18 = *((unsigned int *)t16);
    t15 = (t15 & t18);
    t19 = (t13 + 4);
    t20 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t20 | t14);
    t21 = *((unsigned int *)t19);
    *((unsigned int *)t19) = (t21 | t15);
    xsi_driver_vfirst_trans(t9, 0, 1);
    t22 = (t0 + 9784);
    *((int *)t22) = 1;

LAB1:    return;
}


extern void work_m_01039623975203117176_0267457249_init()
{
	static char *pe[] = {(void *)Always_42_0,(void *)Always_73_1,(void *)implSig1_execute,(void *)implSig2_execute,(void *)implSig3_execute};
	xsi_register_didat("work_m_01039623975203117176_0267457249", "isim/adc_controller_tb.exe.sim/work/m_01039623975203117176_0267457249.didat");
	xsi_register_executes(pe);
}
