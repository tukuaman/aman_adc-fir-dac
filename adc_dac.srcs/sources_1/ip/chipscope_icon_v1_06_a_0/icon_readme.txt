The following files were generated for 'icon' in directory
/home/rock/vlsi_lab/adc_dac/adc_dac.srcs/sources_1/ip/chipscope_icon_v1_06_a_0/

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * icon.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * icon.constraints/icon.ucf
   * icon.constraints/icon.xdc
   * icon.ngc
   * icon.ucf
   * icon.v
   * icon.veo
   * icon.xdc
   * icon_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * icon.asy

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * icon.gise
   * icon.xise

Deliver Readme:
   Readme file for the IP.

   * icon_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * icon_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

