# Tcl script generated by PlanAhead

set reloadAllCoreGenRepositories true

set tclUtilsPath "/opt/Xilinx/14.7/ISE_DS/PlanAhead/scripts/pa_cg_utils.tcl"

set repoPaths ""

set cgIndexMapPath "/home/rock/vlsi_lab/adc_dac/adc_dac.srcs/sources_1/ip/cg_lin_index_map.xml"

set cgProjectPath "/home/rock/vlsi_lab/adc_dac/adc_dac.srcs/sources_1/ip/chipscope_icon_v1_06_a_0/coregen.cgc"

set ipFile "/home/rock/vlsi_lab/adc_dac/adc_dac.srcs/sources_1/ip/chipscope_icon_v1_06_a_0/chipscope_icon_v1_06_a_0.xco"

set ipName "chipscope_icon_v1_06_a_0"

set vlnv "xilinx.com:ip:chipscope_icon:1.06.a"

set cgPartSpec "xc3s500e-4fg320"

set bomFilePath "/home/rock/vlsi_lab/adc_dac/adc_dac.srcs/sources_1/ip/chipscope_icon_v1_06_a_0/pa_cg_bom.xml"

set hdlType "Verilog"

set chains "CUSTOMIZE_CURRENT_CHAIN INSTANTIATION_TEMPLATES_CHAIN"

# configure the IP
set result [source "/opt/Xilinx/14.7/ISE_DS/PlanAhead/scripts/pa_cg_config_core.tcl"]

exit $result

