//`define idle 3'b000 
//`define PROG 3'b001 
//`define ADC  3'b010 
//`define ADC_POS 3'b011 
//`define ADC_NEG 3'b100 

module adc_controller(input rst,input start,input clk_board, input prog, input [7:0] gain, 
                        output reg done, output reg [13:0] miso, 
                        output reg SPI_MOSI, output reg AMP_CS, SPI_SS_B, DAC_CS, SF_CE0, FPGA_INIT_B,
                        SPI_CLK , 
                        AMP_SHDN, AD_CONV, 
                        input AMP_DOUT, input SPI_MISO, 
                        //these for the sake of simulation showing
                        output reg ADC_CS, output reg clk, output reg [7:0]cnt,
                        output reg DAC_CLR);
                        
reg [31:0] dac_out;
reg [3:0] index;
//reg clk;
reg [2:0] state;
reg [33:0] adc_out;
reg [7:0] cnt_temp;
reg [31:0] dac_in;

   parameter idle = 3'b000;
   parameter PROG = 3'b001;
   parameter ADC = 3'b010;
   parameter ADC_POS = 3'b011;
   parameter ADC_NEG = 3'b100;
   parameter DAC = 3'b101;
   parameter DAC_POS = 3'b110;
   parameter DAC_NEG = 3'b111;
   
wire[35:0] control;
icon ico1(.CONTROL0(control));
ila ila1(.CONTROL(control),.CLK(clk_board),.TRIG0(rst),.TRIG1(start),.TRIG2(prog),.TRIG3(gain),
.TRIG4(done),.TRIG5(miso),.TRIG6({SPI_MOSI,SPI_MISO}),.TRIG7({AMP_CS,SPI_SS_B, DAC_CS, SF_CE0, FPGA_INIT_B}),.TRIG8(SPI_CLK),
.TRIG9({clk,AD_CONV}),.TRIG10(dac_out),.TRIG11(AMP_DOUT),.TRIG12(DAC_CLR),.TRIG13(cnt),.TRIG14(state),.TRIG15(adc_out));


reg [2:0] clk_cnt;
always@ (posedge clk_board)
begin
    
    if(rst == 1'b1)
    begin
        clk_cnt <= 0;
        SPI_SS_B =1;
//         DAC_CS=1;
          SF_CE0=1;
           FPGA_INIT_B=0;
    end
    else
    begin
    if (clk_cnt <= 3'd3)
    begin
        clk <= 0;
    end
    else if (clk_cnt <= 3'd6)
    begin
        clk <= 1;
    end
    else if (clk_cnt == 3'd7)
    begin
        clk <= 1;
        clk_cnt <= 3'd0;
    end
    clk_cnt <= clk_cnt + 1;
    end
    
end
  
always@(posedge clk) begin

       if(prog == 1'b1) 
       begin
           state <= PROG;
           AMP_SHDN = 0;          
           cnt = 8'b0;
           done = 0;
           miso = 14'b0; 
           SPI_CLK = 0;
           //DISABLE ADC
           cnt_temp = 0;
           ADC_CS = 1;
           AD_CONV = 0;
       end

       else if(start == 1'b1) begin
           state <= ADC;
//           AMP_SHDN = 1;          
           cnt = 8'b0;
           done = 0;
           miso = 14'b0; 
           SPI_CLK = 0;
           //ENABLE ADC
           cnt_temp =0;
           ADC_CS = 0;
           DAC_CS = 1;
       end

       else 
       begin          
          case(state)
          
          idle: begin           
//           AMP_SHDN = 1;          
           ADC_CS = 1;
           cnt = 8'b0;
           // sit idle
          end
          
          PROG: begin
            
            if(cnt< 5'd16 )
            begin
                if(cnt ==0)
                begin
                    index = 0;
                    AMP_CS = 0;
                    SPI_CLK = 0;                      
                end
                
                cnt_temp = 8'b1; 
            end
                                   
            
            if( cnt != 0 && cnt_temp >0)
                SPI_CLK = ~ SPI_CLK;
            
            if(cnt_temp > 0 && cnt <5'd17 )
            begin
              if(cnt < 5'd16 && SPI_CLK == 0)
              begin
               SPI_MOSI = gain[7 - index];
               index = index + 1;
               end   
               cnt = cnt + 1; 
            end
                      
            
            else if(cnt > 5'd16)
                begin
                state <= idle;         
                AMP_CS = 1;
                cnt = 0;
                cnt_temp = 0;
                done = 1'b1;
                SPI_CLK = 0;
                end
          end 
          
          ADC: begin
//               DAC_CS = 1;
               AD_CONV = 1;
               cnt = 8'b0;
               SPI_CLK = 0;
               miso = 14'b0;
               state <= ADC_POS; 
               end
                
          ADC_POS: begin
            if (cnt == 0)
            begin
                AD_CONV = 0;
                cnt = 1;
            end    
             
            else if(cnt < 8'd35)
                begin
                done = 0;
                SPI_CLK = 1;  
                state <= ADC_NEG;
                adc_out[33 - (cnt-1)] = SPI_MISO;
                miso[13:0] = adc_out[31:18];
                cnt = cnt +1;
                end            
          end
         
          ADC_NEG: begin
            
            if(cnt < 8'd35)
                begin
                done = 0;
                SPI_CLK = 0;  
                state <= ADC_POS;
//                adc_out[33 - (cnt-1)] = SPI_MISO;
//                miso[13:0] = adc_out[31:18];
                end
            else if (cnt < 8'd36)
            begin
                SPI_CLK = 0;
                cnt = cnt +8'd1;
            end
            else
                begin
                    done = 1;
                    //state <= idle;
                   state <= DAC;
                   DAC_CLR = 1;
                   cnt = 8'd0;
                    SPI_CLK = 0;
                    miso[13:0] = adc_out[31:18];
                dac_in[15:4] = miso[13:2];
                end
                
            end

            DAC: begin
                 AD_CONV = 0;
                 DAC_CS = 0;
                 DAC_CLR = 0;
                 cnt = 8'b0;
                 SPI_CLK = 0;
                 state <= DAC_POS; 
                 SPI_MOSI = 0;
                dac_in[23:16] = 8'b00110000;
                 end

             DAC_POS: begin
               if (cnt == 0)
               begin
//                   DAC_CS = 0;
                   DAC_CLR = 1;
                   cnt = 1;
                   state <= DAC_NEG;
               end    
                
               else if(cnt < 8'd33)
                   begin
                   done = 0;
                   dac_out[32 - cnt] = SPI_MISO;
                   SPI_CLK = 1;  
                   state <= DAC_NEG;
                   cnt = cnt +1;
                   end            
             end
                    
             DAC_NEG: begin
               
           if(cnt < 8'd33)
               begin
               done = 0;
               SPI_CLK = 0;  
               state <= DAC_POS;
               SPI_MOSI = dac_in[32 - cnt];
               end
           else if (cnt < 8'd34)
               begin
                   SPI_CLK = 0;
                   cnt = cnt +8'd1;
               end
               else
                   begin
                       done = 1;
                      state <= ADC;
                      cnt = 8'd0;
                       SPI_CLK = 0;
                       //ENABLE ADC
                       cnt_temp =0;
                       DAC_CS = 1;
                       ADC_CS = 0;
                   end
                   
               end
      endcase 

       end
   end

endmodule