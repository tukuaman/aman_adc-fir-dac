`timescale 1ns/1ps
module adc_controller_tb ;
reg rst;
reg start;
reg clk_board, prog;
reg [7:0] gain; 
wire done;
wire [13:0] miso; 
wire SPI_MOSI, AMP_CS, SPI_SCK , AMP_SHDN, AD_CONV; 
reg AMP_DOUT, SPI_MISO;
wire ADC_CS;
wire  SPI_SS_B, DAC_CS, SF_CE0, FPGA_INIT_B;
wire clk;
wire [7:0] cnt;
wire DAC_CLR;
//wire [31:0] dac_out;
adc_controller adc1(rst,start, clk_board,prog,gain,done,miso,SPI_MOSI, AMP_CS, SPI_SS_B,
 DAC_CS, SF_CE0, FPGA_INIT_B,SPI_SCK, 
AMP_SHDN,AD_CONV,AMP_DOUT,SPI_MISO,ADC_CS,clk,cnt,DAC_CLR);

always
begin
#5 clk_board = ~ clk_board;
end

initial
begin
clk_board = 1'b0;rst=1;
# 50 rst =0;
# 160 prog = 1; gain = 8'b01010101; start = 0;
# 80 prog = 0; 
# 1600 start =1 ; prog = 0;
# 80 start = 0; 
# 20000 $finish;

end
endmodule